<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\TemplateVars;

use Interactiv4\Contracts\TemplateVars\Api\TemplateVarInterface;
use Interactiv4\Contracts\TemplateVars\Api\TemplateVarsProviderInterface;

/**
 * Class TemplateVar
 * @api
 */
class TemplateVar implements TemplateVarInterface
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string|null
     */
    private $defaultValue;

    /**
     * @var string|null
     */
    private $value;

    /**
     * @var bool
     */
    private $isCustomizable;

    /**
     * TemplateVar constructor.
     * @param string $name
     * @param string|null $defaultValue
     * @param bool $isCustomizable
     */
    public function __construct(
        string $name,
        ?string $defaultValue = null,
        bool $isCustomizable = true
    ) {
        $this->name = $name;
        $this->value = $this->defaultValue = $defaultValue;
        $this->isCustomizable = $isCustomizable;
    }

    /**
     * @inheritdoc
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @inheritdoc
     */
    public function setValue(?string $value): void
    {
        if (!$this->isCustomizable()) {
            \trigger_error(
                'Notice: Setting a value for a non-customizable template var makes no sense and has no effect.',
                E_USER_NOTICE
            );
            return;
        }
        $this->value = $value;
    }

    /**
     * @inheritdoc
     */
    public function getValue(): ?string
    {
        return !$this->isCustomizable() ? $this->getDefaultValue() : $this->value;
    }

    /**
     * @inheritdoc
     */
    public function getDefaultValue(?TemplateVarsProviderInterface $templateVarsProvider = null): ?string
    {
        return $this->defaultValue;
    }

    /**
     * @inheritdoc
     */
    public function isCustomizable(): bool
    {
        return $this->isCustomizable;
    }

    /**
     * @inheritdoc
     */
    public function __toString(): string
    {
        return (string)$this->getValue();
    }
}
