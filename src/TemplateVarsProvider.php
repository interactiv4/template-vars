<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\TemplateVars;

use Interactiv4\Contracts\TemplateVars\Api\TemplateVarInterface;
use Interactiv4\Contracts\TemplateVars\Api\TemplateVarsProviderInterface;

/**
 * Class TemplateVarsProvider
 * @api
 */
class TemplateVarsProvider implements TemplateVarsProviderInterface
{
    /**
     * @var TemplateVar[]
     */
    private $templateVars = [];

    /**
     * TemplateVarsProvider constructor.
     * @param TemplateVarInterface[] $templateVars
     */
    public function __construct(TemplateVarInterface ...$templateVars)
    {
        $this->templateVars = $templateVars;
    }

    /**
     * @inheritdoc
     */
    public function getTemplateVars(): array
    {
        return $this->templateVars;
    }

    /**
     * @inheritdoc
     */
    public function getCustomizableTemplateVars(): array
    {
        return \array_filter($this->templateVars, function ($templateVar) {
            /** @var TemplateVarInterface $templateVar */
            return $templateVar->isCustomizable();
        });
    }

    /**
     * @inheritdoc
     */
    public function getNonCustomizableTemplateVars(): array
    {
        return \array_filter($this->templateVars, function ($templateVar) {
            /** @var TemplateVarInterface $templateVar */
            return !$templateVar->isCustomizable();
        });
    }

    /**
     * @inheritdoc
     */
    public function getTemplateVarNames(): array
    {
        $templateVarNames = [];
        /** @var TemplateVarInterface $templateVar */
        foreach ($this->templateVars as $templateVar) {
            $templateVarNames[] = $templateVar->getName();
        }

        return $templateVarNames;
    }

    /**
     * @inheritdoc
     */
    public function getCustomizableTemplateVarNames(): array
    {
        $templateVarNames = [];
        /** @var TemplateVarInterface $templateVar */
        foreach ($this->getCustomizableTemplateVars() as $templateVar) {
            $templateVarNames[] = $templateVar->getName();
        }

        return $templateVarNames;
    }

    /**
     * @inheritdoc
     */
    public function getNonCustomizableTemplateVarNames(): array
    {
        $templateVarNames = [];
        /** @var TemplateVarInterface $templateVar */
        foreach ($this->getNonCustomizableTemplateVars() as $templateVar) {
            $templateVarNames[] = $templateVar->getName();
        }

        return $templateVarNames;
    }

    /**
     * @inheritdoc
     */
    public function getTemplateVarByName(string $templateVarName): ?TemplateVarInterface
    {
        foreach ($this->templateVars as $templateVar) {
            if ($templateVarName === $templateVar->getName()) {
                return $templateVar;
            }
        }
        return null;
    }

    /**
     * @inheritdoc
     */
    public function toArray(): array
    {
        $templateVarsArray = [];
        foreach ($this->templateVars as $templateVar) {
            $templateVarsArray[$templateVar->getName()] = $templateVar->getValue();
        }

        return $templateVarsArray;
    }
}
