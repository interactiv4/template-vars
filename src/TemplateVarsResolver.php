<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\TemplateVars;

use Interactiv4\Contracts\TemplateVars\Api\TemplateVarsProviderInterface;
use Interactiv4\Contracts\TemplateVars\Api\TemplateVarsResolverInterface;

/**
 * Class TemplateVarsResolver
 * @api
 */
class TemplateVarsResolver implements TemplateVarsResolverInterface
{
    const TEMPLATE_VAR_PLACEHOLDER_START = '{{';

    const TEMPLATE_VAR_PLACEHOLDER_END = '}}';

    /**
     * @var string
     */
    private $placeholderStart;

    /**
     * @var string
     */
    private $placeholderEnd;

    /**
     * TemplateVarsResolver constructor.
     * @param string|null $placeholderStart
     * @param string|null $placeholderEnd
     */
    public function __construct(
        string $placeholderStart = null,
        string $placeholderEnd = null
    ) {
        $this->placeholderStart = $placeholderStart ?? static::TEMPLATE_VAR_PLACEHOLDER_START;
        $this->placeholderEnd = $placeholderEnd ?? static::TEMPLATE_VAR_PLACEHOLDER_END;
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        TemplateVarsProviderInterface $templateVarsProvider,
        string $targetString
    ): string {
        foreach ($templateVarsProvider->toArray() as $templateVarName => $templateVarValue) {
            $templateVarPlaceholder = \implode(
                '',
                [
                    $this->placeholderStart,
                    $templateVarName,
                    $this->placeholderEnd,
                ]
            );
            $targetString = \str_replace($templateVarPlaceholder, $templateVarValue, $targetString);
        }

        return $targetString;
    }
}
