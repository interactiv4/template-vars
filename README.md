# Interactiv4 Template Vars

Description
-----------
This package provides implementation for interactiv4/template-vars-contracts package.


Versioning
----------
This package follows semver versioning.


Compatibility
-------------
- PHP ^7.1


Installation Instructions
-------------------------
It can be installed manually using composer, by typing the following command:

`composer require interactiv4/template-vars --update-with-all-dependencies`


Support
-------
Refer to [issue tracker](https://bitbucket.org/interactiv4/template-vars/issues) to open an issue if needed.


Credits
---------
Supported and maintained by Interactiv4 Team.


Contribution
------------
Any contribution is highly appreciated.
The best way to contribute code is to open a [pull request on Bitbucket](https://bitbucket.org/interactiv4/template-vars/pull-requests/new).


License
-------
[MIT](https://es.wikipedia.org/wiki/Licencia_MIT)


Copyright
---------
Copyright (c) 2019 Interactiv4 S.L.