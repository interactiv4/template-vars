<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 * @SuppressWarnings(PHPMD)
 */

namespace Interactiv4\TemplateVars\Test;

use Interactiv4\Contracts\TemplateVars\Api\TemplateVarInterface;
use Interactiv4\Contracts\TemplateVars\Api\TemplateVarsProviderInterface;
use Interactiv4\TemplateVars\TemplateVarsProvider;
use PHPUnit\Framework\TestCase;

/**
 * Class TemplateVarsProviderTest
 */
class TemplateVarsProviderTest extends TestCase
{
    /**
     * Test TemplateVarsProvider class exists and is an instance of TemplateVarsProviderInterface
     */
    public function testInstanceOf()
    {
        $templateVarsProvider = new TemplateVarsProvider();
        $this->assertInstanceOf(TemplateVarsProviderInterface::class, $templateVarsProvider);
    }

    /**
     * Test get template vars methods
     */
    public function testGetTemplateVars()
    {
        $templateVarsProvider = $this->getConfiguredTemplateVarsProvider();
        $this->assertCount(5, $templateVarsProvider->getTemplateVars());
        $this->assertCount(3, $templateVarsProvider->getCustomizableTemplateVars());
        $this->assertCount(2, $templateVarsProvider->getNonCustomizableTemplateVars());
    }

    /**
     * Test get template var names methods
     */
    public function testGetTemplateVarNames()
    {
        $templateVarsProvider = $this->getConfiguredTemplateVarsProvider();
        $this->assertEquals(['A', 'B', 'C', 'D', 'E'], $templateVarsProvider->getTemplateVarNames());
        $this->assertEquals(['A', 'C', 'E'], $templateVarsProvider->getCustomizableTemplateVarNames());
        $this->assertEquals(['B', 'D'], $templateVarsProvider->getNonCustomizableTemplateVarNames());
    }

    /**
     * Test getTemplateVarByName
     */
    public function testGetTemplateVarByName()
    {
        $templateVarsProvider = $this->getConfiguredTemplateVarsProvider();
        $this->assertEquals('A', $templateVarsProvider->getTemplateVarByName('A')->getName());
        $this->assertEquals('B', $templateVarsProvider->getTemplateVarByName('B')->getName());
        $this->assertEquals('C', $templateVarsProvider->getTemplateVarByName('C')->getName());
        $this->assertEquals('D', $templateVarsProvider->getTemplateVarByName('D')->getName());
        $this->assertEquals('E', $templateVarsProvider->getTemplateVarByName('E')->getName());
        $this->assertNull($templateVarsProvider->getTemplateVarByName('Z'));
    }

    /**
     * Test getTemplateVarByName
     */
    public function testToArray()
    {
        $templateVarsProvider = $this->getConfiguredTemplateVarsProvider();
        $this->assertEquals(
            [
                'A' => 'Value A',
                'B' => 'Value B',
                'C' => 'Value C',
                'D' => 'Value D',
                'E' => 'Value E'
            ],
            $templateVarsProvider->toArray()
        );
    }

    /**
     * @return TemplateVarsProvider
     */
    private function getConfiguredTemplateVarsProvider(): TemplateVarsProvider
    {
        $templateVarMockA = $this->getMockForAbstractClass(TemplateVarInterface::class);
        $templateVarMockB = $this->getMockForAbstractClass(TemplateVarInterface::class);
        $templateVarMockC = $this->getMockForAbstractClass(TemplateVarInterface::class);
        $templateVarMockD = $this->getMockForAbstractClass(TemplateVarInterface::class);
        $templateVarMockE = $this->getMockForAbstractClass(TemplateVarInterface::class);

        $templateVarMockA->expects($this->any())->method('getName')->willReturn('A');
        $templateVarMockA->expects($this->any())->method('getValue')->willReturn('Value A');
        $templateVarMockA->expects($this->any())->method('isCustomizable')->willReturn(true);

        $templateVarMockB->expects($this->any())->method('getName')->willReturn('B');
        $templateVarMockB->expects($this->any())->method('getValue')->willReturn('Value B');
        $templateVarMockB->expects($this->any())->method('isCustomizable')->willReturn(false);

        $templateVarMockC->expects($this->any())->method('getName')->willReturn('C');
        $templateVarMockC->expects($this->any())->method('getValue')->willReturn('Value C');
        $templateVarMockC->expects($this->any())->method('isCustomizable')->willReturn(true);

        $templateVarMockD->expects($this->any())->method('getName')->willReturn('D');
        $templateVarMockD->expects($this->any())->method('getValue')->willReturn('Value D');
        $templateVarMockD->expects($this->any())->method('isCustomizable')->willReturn(false);

        $templateVarMockE->expects($this->any())->method('getName')->willReturn('E');
        $templateVarMockE->expects($this->any())->method('getValue')->willReturn('Value E');
        $templateVarMockE->expects($this->any())->method('isCustomizable')->willReturn(true);

        return new TemplateVarsProvider(
            $templateVarMockA,
            $templateVarMockB,
            $templateVarMockC,
            $templateVarMockD,
            $templateVarMockE
        );
    }
}
