<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 * @SuppressWarnings(PHPMD)
 */

namespace Interactiv4\TemplateVars\Test;

use Interactiv4\Contracts\TemplateVars\Api\TemplateVarsProviderInterface;
use Interactiv4\Contracts\TemplateVars\Api\TemplateVarsResolverInterface;
use Interactiv4\TemplateVars\TemplateVarsResolver;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * Class TemplateVarsResolverTest
 */
class TemplateVarsResolverTest extends TestCase
{
    /**
     * Test TemplateVarsResolver class exists and is an instance of TemplateVarsResolverInterface
     */
    public function testInstanceOf()
    {
        $templateVarsResolver = new TemplateVarsResolver();
        $this->assertInstanceOf(TemplateVarsResolverInterface::class, $templateVarsResolver);
    }

    /**
     * Test it resolves string with template vars properly
     * @param string $templateValueThingName
     * @param string $templateValueThingType
     * @param string $templateValuePlaceName
     * @param string $expectedResolvedString
     * @dataProvider resolveDataProvider
     */
    public function testResolveStringWithTemplateVars(
        string $templateValueThingName,
        string $templateValueThingType,
        string $templateValuePlaceName,
        string $expectedResolvedString
    ) {
        /** @var TemplateVarsProviderInterface|MockObject $templateVarsProviderMock */
        $templateVarsProviderMock = $this->getMockForAbstractClass(TemplateVarsProviderInterface::class);
        $templateVarsProviderMock->expects($this->any())->method('toArray')->willReturn(
            [
                'thing-name' => $templateValueThingName,
                'thing-type' => $templateValueThingType,
                'place-name' => $templateValuePlaceName,
            ]
        );

        $templateVarsResolver = new TemplateVarsResolver();

        $resolvedString = $templateVarsResolver->resolve(
            $templateVarsProviderMock,
            TemplateVarsResolver::TEMPLATE_VAR_PLACEHOLDER_START .
            'thing-name' .
            TemplateVarsResolver::TEMPLATE_VAR_PLACEHOLDER_END .
            ' is ' .
            TemplateVarsResolver::TEMPLATE_VAR_PLACEHOLDER_START .
            'thing-type' .
            TemplateVarsResolver::TEMPLATE_VAR_PLACEHOLDER_END .
            ' that grows in ' .
            TemplateVarsResolver::TEMPLATE_VAR_PLACEHOLDER_START .
            'place-name' .
            TemplateVarsResolver::TEMPLATE_VAR_PLACEHOLDER_END

        );
        $this->assertEquals($expectedResolvedString, $resolvedString);
    }

    /**
     * Test it resolves string with template vars properly with custom placeholders
     * @param string $templateValueThingName
     * @param string $templateValueThingType
     * @param string $templateValuePlaceName
     * @param string $expectedResolvedString
     * @dataProvider resolveDataProvider
     */
    public function testResolveStringWithTemplateVarsWithCustomPlaceholders(
        string $templateValueThingName,
        string $templateValueThingType,
        string $templateValuePlaceName,
        string $expectedResolvedString
    ) {
        $placeholderStart = '((';
        $placeholderEnd = '))';

        /** @var TemplateVarsProviderInterface|MockObject $templateVarsProviderMock */
        $templateVarsProviderMock = $this->getMockForAbstractClass(TemplateVarsProviderInterface::class);
        $templateVarsProviderMock->expects($this->any())->method('toArray')->willReturn(
            [
                'thing-name' => $templateValueThingName,
                'thing-type' => $templateValueThingType,
                'place-name' => $templateValuePlaceName,
            ]
        );

        $templateVarsResolver = new TemplateVarsResolver($placeholderStart, $placeholderEnd);

        $resolvedString = $templateVarsResolver->resolve(
            $templateVarsProviderMock,
            $placeholderStart .
            'thing-name' .
            $placeholderEnd .
            ' is ' .
            $placeholderStart .
            'thing-type' .
            $placeholderEnd .
            ' that grows in ' .
            $placeholderStart .
            'place-name' .
            $placeholderEnd

        );
        $this->assertEquals($expectedResolvedString, $resolvedString);
    }

    /**
     * @return array
     */
    public function resolveDataProvider(): array
    {
        return [
            [
                'A car',
                'a machine',
                'the trees',
                'A car is a machine that grows in the trees'
            ],
            [
                'A squid',
                'an animal',
                'sandwiches',
                'A squid is an animal that grows in sandwiches'
            ],
            [
                'A developer',
                'a social misfit',
                'dark and wet places',
                'A developer is a social misfit that grows in dark and wet places'
            ],
        ];
    }
}
