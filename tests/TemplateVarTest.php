<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 * @SuppressWarnings(PHPMD)
 */

namespace Interactiv4\TemplateVars\Test;

use Interactiv4\Contracts\TemplateVars\Api\TemplateVarInterface;
use Interactiv4\Contracts\TemplateVars\Api\TemplateVarsProviderInterface;
use Interactiv4\TemplateVars\TemplateVar;
use PHPUnit\Framework\TestCase;

/**
 * Class TemplateVarsTest
 */
class TemplateVarsTest extends TestCase
{
    /**
     * Test it requires name in constructor
     */
    public function testItRequiresNameInConstructor()
    {
        if (!\class_exists('ArgumentCountError')) {
            $this->markTestSkipped('This test only applies to php >= 7.1');
        }
        $this->expectException(\ArgumentCountError::class);
        new TemplateVar();
    }

    /**
     * Test TemplateVar class exists and is an instance of TemplateVarInterface
     */
    public function testInstanceOf()
    {
        $templateVar = new TemplateVar('template-var-name');
        $this->assertInstanceOf(TemplateVarInterface::class, $templateVar);
    }

    /**
     * Test default values
     */
    public function testDefaultValues()
    {
        $templateVar = new TemplateVar('template-var-name');
        $this->assertEquals('template-var-name', $templateVar->getName());
        $this->assertNull($templateVar->getDefaultValue());
        $this->assertNull($templateVar->getValue());
        $this->assertTrue($templateVar->isCustomizable());
    }

    /**
     * Test default values
     */
    public function testValuesInConstructor()
    {
        // Non-customizable
        $templateVar = new TemplateVar('template-var-name', 'default-value', false);
        $this->assertEquals('template-var-name', $templateVar->getName());
        $this->assertEquals('default-value', $templateVar->getDefaultValue());
        $this->assertEquals('default-value', $templateVar->getValue());
        $this->assertFalse($templateVar->isCustomizable());

        // Customizable
        $templateVar = new TemplateVar('template-var-name', 'default-value', true);
        $this->assertEquals('template-var-name', $templateVar->getName());
        $this->assertEquals('default-value', $templateVar->getDefaultValue());
        $this->assertEquals('default-value', $templateVar->getValue());
        $this->assertTrue($templateVar->isCustomizable());
    }

    /**
     * Test getDefaultValue() accepts template vars provider but do not use it by default
     */
    public function testGetDefaultValueAcceptsTemplateVarsProviderButDoNotUseItByDefault()
    {
        $templateVar = new TemplateVar('template-var-name');
        $templateVarsProvider = $this->getMockForAbstractClass(TemplateVarsProviderInterface::class);
        $templateVarsProvider->expects($this->never())->method($this->anything());
        $this->assertNull($templateVar->getDefaultValue($templateVarsProvider));
    }

    /**
     * Test set value not allowed when non customizable
     */
    public function testSetValueNotAllowedWhenNonCustomizable()
    {
        $this->expectException(\PHPUnit\Framework\Error\Notice::class);
        $templateVar = new TemplateVar('template-var-name', 'default-value', false);

        // Test it throws notice and still returns default value
        $templateVar->setValue('custom-value');
        $this->assertEquals($templateVar->getDefaultValue(), $templateVar->getValue());
    }

    /**
     * Test set value not allowed when non customizable
     */
    public function testSetValueAllowedWhenCustomizable()
    {
        $templateVar = new TemplateVar('template-var-name', 'default-value', true);

        // Test it changes value
        $templateVar->setValue('custom-value');
        $this->assertEquals('custom-value', $templateVar->getValue());
    }

    /**
     * Test __toString
     */
    public function testToString()
    {
        $templateVar = new TemplateVar('template-var-name', 'default-value', true);

        $this->assertEquals($templateVar->getValue(), $templateVar->__toString());
        $this->assertEquals($templateVar->getValue(), (string)$templateVar);
    }
}
